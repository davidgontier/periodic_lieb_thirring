####################################
# 2d periodic LT
# Written by D. Gontier on September 8 2020

import numpy as np
import numpy.fft as fft
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special
import math
import itertools
from matplotlib import ticker, cm


##############################
# This is the code for the 2dimensional LT
##############################

def myprint(s, logfile):
    print(s)
    if logfile != None:
        with open(logfile, "a") as file:
            file.write("\n"+s)

###############################
# Brillouin zone
##############################

class BZ:
    def __init__(self, Nq):

        self.Nq = Nq
        self.type = type

        self.v1 = np.array([2*np.pi, 0, 0]) 
        self.v2 = np.array([0, 2*np.pi, 0]) 
        self.v3 = np.array([0, 0, 2*np.pi])
        
        self.BZ = self.get_BZ()
        self.rBZ, self.weights = self.get_rBZ()
        self.Nb = None
        self.sparse = None


    def get_BZ(self):
        BZ = []
        qq = np.linspace(0, 1-1/self.Nq, self.Nq)
        for q1, q2, q3 in itertools.product(qq, qq, qq):
                q = q1*self.v1 + q2*self.v2 + q3*self.v3
                BZ.append(q)
        return BZ
    
    def get_irrKpts(self, q):
        # returns the irreducible kpts.
        newq = np.minimum(q, 2*np.pi*np.array([1,1,1]) - q)
        newq.sort()
        return newq

    def get_rBZ(self):
        rBZ = []
        weights = {}

        tol=1e-7
        # We restrict our attention to potential that are 
        # 1/ even V(x1, x2) = V(-x1, x2), and 
        # 2/ Invariant by pi/4 rotations: V(Rx) = V(x)
        for q in self.BZ:
            rq = self.get_irrKpts(q)
            if np.linalg.norm(q - rq) < tol:
                rBZ.append(q)
            key = tuple(np.floor(rq*1e6))
            if key in weights:
                weights[key] += 1
            else:
                weights[key] = 1

        w = [] # the weights as a list
        for rq in rBZ:
            key = tuple(np.floor(rq*1e6))
            w.append(weights[key])
        w = np.array(w)
        return np.array(rBZ), w/sum(w)


    ##############################
    # The reduced Brillouin zone is linked to the symmetry of the system
    def average_transform(self, u):
        # returns the average of all transformations (of the rBZ)
        # Here, u[i,j,k] = u[perm(i,j,k)] for all permutations

        u1 = u + np.flip(u, 0)
        u2 = u1 + np.flip(u1, 1)
        u3 = u2 + np.flip(u2, 2)

        newu = np.zeros( np.shape(u) )

        for perm in itertools.permutations([0,1,2]):
            newu += np.transpose(u3, perm)

        return np.real(newu)/48

    def get_mDelta_q(self, q, Nb, sparse=True):
        # return (- i \nabla + q)^2 in Fourier
        # The operation is diagonal in Fourier
        # This is the multiplication by kinetic_q 
    
        # Construct kinetic_q
        KK = np.linspace(0, Nb-1, Nb)
        v1, v2, v3 = self.v1, self.v2, self.v3

        _kinetic_q = np.zeros((Nb, Nb, Nb))
        # TODO parallel this part...

        for kx, ky, kz in itertools.product(range(Nb), range(Nb), range(Nb)):
            n0 = np.linalg.norm( kx*v1      + ky*v2         + kz*v3         - q )**2
            n1 = np.linalg.norm( (kx-Nb)*v1 + ky*v2         + kz*v3         - q )**2
            n2 = np.linalg.norm( kx*v1      + (ky-Nb)*v2    + kz*v3         - q )**2
            n3 = np.linalg.norm( kx*v1      + ky*v2         + (kz-Nb)*v3    - q )**2
            n4 = np.linalg.norm( (kx-Nb)*v1 + (ky-Nb)*v2    + kz*v3         - q )**2
            n5 = np.linalg.norm( kx*v1      + (ky-Nb)*v2    + (kz-Nb)*v3    - q )**2
            n6 = np.linalg.norm( (kx-Nb)*v1 + (ky-Nb)*v2    + (kz-Nb)*v3    - q )**2
            n7 = np.linalg.norm( (kx-Nb)*v1 + (ky-Nb)*v2    + (kz-Nb)*v3    - q )**2
            _kinetic_q[kx, ky, kz] = np.amin([n0, n1, n2, n3, n4, n5, n6, n7])

        def mult_mDelta(u):
            ul = np.reshape(u, (Nb, Nb, Nb))
            U = fft.ifftn(ul)
            mDeltaU = _kinetic_q*U
            mdeltaul = fft.fftn(mDeltaU)
            mdeltau = np.reshape(mdeltaul, Nb**3)
            return mdeltau
        if sparse:
            return LA.LinearOperator((Nb**3, Nb**3), matvec = mult_mDelta)
        else:
            def mDelta():
                A = np.zeros((Nb**3, Nb**3), dtype='complex')
                for i in range(Nb**3):
                    u = np.zeros(Nb**3)
                    u[i] = 1
                    v = mult_mDelta(u)
                    A[:,i] = v
                return A
            return mDelta()

    ############################
    def compute_mDeltaq(self, Nb, sparse = True):
        if self.Nb == Nb and self.sparse == sparse:
            return
        
        self.Nb, self.sparse = Nb, sparse
        self.list_mDeltaq = []

        for q in self.rBZ:
            mDeltaq = self.get_mDelta_q(q, Nb, sparse)
            self.list_mDeltaq.append(mDeltaq)

##################################
# The multiplication by V operator
def get_Vop(V, sparse = True):
    Nb = np.shape(V)[0]
    Vl = np.reshape(V, Nb**3)

    if sparse:
        def mult_V(ul):
            u = np.reshape(ul, (Nb, Nb, Nb))
            Vu = V*u
            return np.reshape(Vu, Nb**3)
        return LA.LinearOperator((Nb**3, Nb**3), matvec = mult_V)
    else:
        return np.diag(Vl)

def get_V0(rBZ, I, Nb):
    # returns a correct first initial potential for neigs = 1
    tt = np.linspace(0, 1-1/Nb, Nb)
    xgrid, ygrid, zgrid = np.meshgrid(tt, tt, tt)
   
    u = (np.cos(np.pi*xgrid) * np.cos(np.pi*ygrid) * np.cos(np.pi*zgrid) )**2
    Ru = rBZ.average_transform(u)
    Ru += np.roll ( np.roll ( np.roll(Ru, Nb//2, axis = 0), Nb//2, axis = 1), Nb//2, axis = 2)
    finalu = rBZ.average_transform(Ru)
    return -12*I*finalu

def Lp_norm(V, p):
    Nb = np.shape(V)[0]
    I = np.sum(abs(V)**p)/(Nb**3)
    return I**(1/p)

#######################################
# The LT semi-classical constant
def Lsc(kappa, d = 3):
    return 1/(2**d * np.pi**(d/2) )*special.gamma(kappa+1)/special.gamma(kappa+d/2+1) #semi classical cst


def compute_rho(V, rBZ, kappa, neigs = 2, logfile = None, d = 3, sparse = True):
    # V is the potential evaluated on a grid
    # returns the potential rho, the sum of the eigenvalues**kappa, and the kinetic energy
    
    Nb, Vop = np.shape(V)[0], get_Vop(V, sparse)
    rBZ.compute_mDeltaq(Nb, sparse)

    rho = np.zeros((Nb, Nb, Nb))
    sum_eigs = 0
    kinetic = 0

    v0 = np.ones(Nb**3) #Starting vector for diagonalization

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        if sparse:
            w, v = LA.eigsh(H, neigs, v0 = v0, which='SA', tol=1e-8)
        else:
            w, v = np.linalg.eigh(H)

        for ie in range(neigs):
            lambdaq, uq = w[ie], v[:,ie] # only first negative eigenvalues
            uq = uq*Nb**(3/2) # normalise so that integral uq = 1

            K = np.real( np.dot(np.conj(uq), mDeltaq.dot(uq)) )

            if lambdaq < 0:
                kinetic +=  abs(lambdaq)**(kappa - 1) * K * rBZ.weights[iq]
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
                rho += abs(lambdaq)**(kappa - 1)* np.reshape(abs(uq)**2, (Nb,Nb,Nb)) * rBZ.weights[iq]

    return rBZ.average_transform(rho), sum_eigs, kinetic


################################
def get_info(V, rBZ, kappa, neigs = 2, sparse = True):
    Nb, Vop = np.shape(V)[0], get_Vop(V, sparse)
    rBZ.compute_mDeltaq(Nb, sparse)

    eigs = np.zeros( ( len(rBZ.rBZ) , neigs) )
    sum_eigs = 0

    v0 = np.ones(Nb**3) #Starting vector for diagonalization

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        if sparse:
            w, v = LA.eigsh(H, neigs, v0 = v0, which='SA', tol=1e-8)
        else:
            w, v = np.linalg.eigh(H)

        eigs[iq, :] = w[:neigs]

        for ie in range(neigs):
            lambdaq = w[ie]
            if lambdaq < 0:
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
    #
    d = 3
    integral = Lp_norm(V, kappa+d/2)
    Llt = sum_eigs/(integral**(kappa + d/2))
    print("For kappa = {}".format(kappa))
    print("sum_eigs = {}, integral = {}, L/Lsc > {}".format( sum_eigs, integral, Llt/Lsc(kappa)))
    return eigs, Llt

#############################


def LT_constraint(V0, rBZ, kappa, integral, neigs = 2, tol=1e-6, Niter = 20, logfile=None, sparse = True) :
    # Returns V, sum_eigs, integral.
    # V is the self-consistent optimal potential
    # sum_eigs is \int |\lambda(q)|^\kappa
    # integral is \| V \|_{\kappa + 1}
    # L is therefore greater than sum_eigs/I

    # Initialisation
    d = 3
    Nb = np.shape(V0)[0]
    rBZ.compute_mDeltaq(Nb, sparse)

    print( "\nOptimisation with constraint integral = {}, and kappa = {}.".format(integral, kappa))
    print( "Lsc = {}".format( Lsc(kappa))) 
    

    In = Lp_norm(V0, kappa+d/2)
    Vn = (integral/In)*V0 #normalize V.
    Llt = 0

    rhon, sum_eigs, _ = compute_rho(Vn, rBZ, kappa, logfile=logfile, sparse=sparse)
    list_V, list_Llt = [], []

    # main loop
    for n in range(Niter):
        Vnp1 = - rhon**(1/(kappa + d/2 - 1))
        # normalisation
        In = Lp_norm(Vnp1, kappa+d/2)
        Vnp1 = (integral/In)*Vnp1

        rhon, sum_eigs, _ = compute_rho(Vnp1, rBZ, kappa, neigs, logfile, sparse = sparse)
        Lltp1 = sum_eigs/(integral**(kappa+d/2))

        print("\tIteration {:2d}, I = {:.3f}, kappa = {}, Vmax - Vmin = {:.6f}, LT/Lsc = {:.8f}".format(n, integral, kappa, np.amax(Vnp1) - np.amin(Vnp1), Lltp1/Lsc(kappa)))
        
        if np.linalg.norm(Vnp1 - Vn)/Nb < tol or abs(Lltp1 - Llt)/Lsc(kappa) < 1e-8:
            print("Convergence after {} iterations".format(n))
            return Vnp1, Lltp1, list_V, list_Llt
        
        list_V.append(Vn)
        list_Llt.append(Llt)
        Vn, Llt = Vnp1, Lltp1
    print("No convergence after {} iterations".format(Niter))
    return Vnp1, Llt, list_V, list_Llt