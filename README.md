# The periodic Lieb-Thirring inequality.

This is the code + pictures of the article:

*The periodic Lieb-Thirring inequality.*
R.L. Frank, D. Gontier, M. Lewin
Partial Differential Equations, Spectral Theory, and Mathematical Physics. The Ari Laptev Anniversary Volume, volume 18 of EMS Series of Congress Reports, chapter The periodic Lieb–Thirring inequality, pages 135–154. EMS Publishing House, June 2021.

Book version: https://ems.press/books/ecr/187
Arxiv version: https://arxiv.org/abs/2010.02981

Author: David Gontier (2021)