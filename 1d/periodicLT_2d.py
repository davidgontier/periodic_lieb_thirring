####################################
# 2d periodic LT
# Written by D. Gontier on September 8 2020

import numpy as np
import numpy.fft as fft
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special
import math
from matplotlib import ticker, cm


##############################
# This is the code for the 2dimensional LT
##############################

def myprint(s, logfile):
    print(s)
    if logfile != None:
        with open(logfile, "a") as file:
            file.write("\n"+s)

###############################
# Brillouin zone
##############################

class BZ:
    def __init__(self, Nq, type="Triangular"):
        # type can be "Triangular" or "Square"

        self.Nq = Nq
        self.type = type

        # The dual lattice  
        if type == "Triangular" or type == "Hexagonal": #same lattice
            a = np.sqrt(2/np.sqrt(3))  
            self.v1 = 2*np.pi*np.array([1, -1/np.sqrt(3)]) / a
            self.v2 = 2*np.pi*np.array([0, 2/np.sqrt(3)]) / a

        elif type == "Square":
             # The dual lattice
            self.v1 = np.array([0, 2*np.pi]) 
            self.v2 = np.array([2*np.pi, 0]) 
        
        self.BZ = self.get_BZ()
        self.rBZ = self.get_rBZ()
        self.weights = self.get_weights()
        self.Nb = None


    def get_BZ(self):
        BZ = []
        qq = np.linspace(0, 1-1/self.Nq, self.Nq)
        for q1 in qq:
           for q2 in qq:
                q = q1*self.v1 + q2*self.v2
                BZ.append(q)    
        return BZ


    def get_rBZ(self):
        rBZ = []
        tol=1e-7
    
        if self.type == "Triangular" or self.type == "Hexagonal":
            # We restrict our attention to potential that are 
            # 1/ even V(x1, x2) = V(-x1, x2), and 
            # 2/ Invariant by 2\pi/3 rotations: V(Rx) = V(x)
            for q in self.BZ:
                if q[0] > -tol and q[0] < q[1]/np.sqrt(3)+tol and q[1] < self.v2[1]/2+tol:
                    rBZ.append(q)

        elif self.type == "Square":
            # We restrict our attention to potential that are 
            # 1/ even V(x1, x2) = V(-x1, x2), and 
            # 2/ Invariant by pi/4 rotations: V(Rx) = V(x)
            for q in self.BZ:
                if q[0] < np.pi+tol and q[1] < q[0] + tol:
                    rBZ.append(q)
        return rBZ



    
    def get_weights(self):
        weights = []
        tol = 1e-7

        if self.type == "Triangular" or self.type == "Hexagonal":
            for q in self.rBZ:
                # first the special points
                if np.linalg.norm(q) < tol: #the center point
                    weights.append(1)
                elif np.linalg.norm(q - self.v2-(self.v1 - self.v2)/3) < tol: # the "corner" of the BZ
                    weights.append(2)
                elif np.linalg.norm(q - self.v2/2) < tol: # the "middle segment" of the BZ
                    weights.append(3)
                elif abs(q[0]) < tol: # the horizontal line
                    weights.append(6)
                elif abs(q[1] - np.sqrt(3)*q[0]) < tol: # the "diagonal" line
                    weights.append(6)
                elif abs(q[1] - self.v2[1]/2) < tol: # the upper line
                    weights.append(6)
                else:
                    weights.append(12)

        elif self.type == "Square":
            for q in self.rBZ:
                # first the special points
                if np.linalg.norm(q) < tol: #the center point
                    weights.append(1)
                elif np.linalg.norm(q - np.array([np.pi, np.pi])) < tol: # the "central point"
                    weights.append(1)
                elif np.linalg.norm(q - np.array([np.pi, 0])) < tol: # the middle bottom point
                    weights.append(2)
                elif abs(q[1]) < tol: # the horizontal line
                    weights.append(4)
                elif abs(q[0] - np.pi) < tol: # the vertical line
                    weights.append(4)
                elif abs(q[0] - q[1]) < tol: # the "diagonal" line
                    weights.append(4)
                else:
                    weights.append(8)

        return np.array(weights)/(self.Nq**2) #(normalisation)


    ##############################
    # The reduced Brillouin zone is linked to the symmetry of the system

    def reflect(self, u):
        # u is defined on the WS grid
        Nb = np.shape(u)[0]
        newu = np.zeros((Nb,Nb))

        if self.type=="Triangular" or self.type == "Hexagonal":
            # transforms a1 --> a1 and a2 --> a1 - a2
            for i in range(Nb):
                for j in range(Nb):
                    newu[i,j] = u[(-i)%Nb, (i+j)%Nb]

        elif self.type == "Square":
            # transforms a1 --> a1 and a2 --> - a2
            for i in range(Nb):
                for j in range(Nb):
                    newu[i,j] = u[i%Nb, (-j-1)%Nb]
        
        return newu

    def rotate(self, u):
        Nb = np.shape(u)[0]
        newu = np.zeros((Nb, Nb))

        if self.type == "Triangular" or self.type == "Hexagonal":
            # rotation by 2*\pi/3 
            # transforms a1 --> a2-a1 and a2 --> -a1.
            for i in range(Nb):
                for j in range(Nb):
                    newu[i,j] = u[(-i-j)%Nb, i%Nb]

        elif self.type == "Square":
            # rotation by pi/4
            for i in range(Nb):
                for j in range(Nb):
                    newu[i,j] = u[j%Nb, (-i-1)%Nb]

        return newu

    def average_transform(self, u): 
        # returns the average of all transformations (of the rBZ)
        
        if self.type == "Triangular" or self.type == "Hexagonal": # 12 transformations
            R1u = self.rotate(u)
            R2u = self.rotate(R1u)

            newu = u.copy() + R1u + R2u # rotations
            newu += self.reflect(newu) # 
            return 2*np.real(newu)/12

        elif self.type == "Square": # 8 transformations
            Ru = self.rotate(u)
            RRu = self.rotate(Ru)
            RRRu = self.rotate(RRu)
            newu = u.copy() + Ru + RRu + RRRu
            newu += self.reflect(newu)
            return np.real(newu)/8

    def get_mDelta_q(self, q, Nb):
        # return (- i \nabla + q)^2 in Fourier
        # The operation is diagonal in Fourier
        # This is the multiplication by kinetic_q 
    
        # Construct kinetic_q
        KK = np.linspace(0, Nb-1, Nb)
        kx, ky = np.meshgrid(KK, KK)
        v1, v2 = self.v1, self.v2
        k = q

        n1 = (kx*v1[0] + ky*v2[0] - k[0])**2 + (kx*v1[1] + ky*v2[1] - k[1])**2
        n2 = ((kx-Nb)*v1[0] + ky*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + ky*v2[1] - k[1])**2
        n3 = (kx*v1[0] + (ky-Nb)*v2[0] - k[0])**2 + (kx*v1[1] + (ky - Nb)*v2[1]- k[1])**2
        n4 = ((kx-Nb)*v1[0] + (ky - Nb)*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + (ky - Nb)*v2[1] - k[1])**2

        _kinetic_q = np.minimum( np.minimum(n1, n2), np.minimum(n3, n4))

        def mult_mDelta(u):
            ul = np.reshape(u, (Nb, Nb))
            U = fft.ifft2(ul)
            mDeltaU = _kinetic_q*U
            mdeltaul = fft.fft2(mDeltaU)
            mdeltau = np.reshape(mdeltaul, Nb**2)
            return mdeltau

        return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_mDelta)

    ############################
    def compute_mDeltaq(self, Nb):
        if self.Nb == Nb:
            return
        
        self.Nb = Nb
        self.list_mDeltaq = []
        for q in self.rBZ:
            mDeltaq = self.get_mDelta_q(q, Nb)
            self.list_mDeltaq.append(mDeltaq)


#######################################
# A class for WS. We encode the different transformations
#######################################
class WS():

    def __init__(self, Nb, type="Triangular"):

        self.Nb = Nb
        self.type = type

        if type == "Triangular" or type == "Hexagonal":
            a = np.sqrt(2/np.sqrt(3))
            self.a1 = a*np.array([1,0])
            self.a2 = a*np.array([1/2, np.sqrt(3)/2])
            self.s = 4 # the number of periodic cells to plot per direction

        elif type == "Square":
            self.a1 = np.array([1, 0])
            self.a2 = np.array([0, 1])
            self.s = 3
        
        xx = np.linspace(0, 1-1/Nb, Nb)
        xgrid, ygrid = np.meshgrid(xx, xx)
        self.xWS = xgrid*self.a1[0] + ygrid*self.a2[0]
        self.yWS = xgrid*self.a1[1] + ygrid*self.a2[1]

        xxplot = np.linspace(0, self.s-1-1/Nb, self.s*Nb) #expand in 3 dimensions to plot
        xplotgrid, yplotgrid = np.meshgrid(xxplot, xxplot)
        self.xWSplot = xplotgrid*self.a1[0] + yplotgrid*self.a2[0]
        self.yWSplot = xplotgrid*self.a1[1] + yplotgrid*self.a2[1]

    def plot(self,u, ax = None):
        Nb = np.shape(u)[0]
        uplot = np.zeros((self.s*Nb, self.s*Nb))
        for i in range(self.s):
            for j in range(self.s):
                uplot[i*Nb:(i+1)*Nb, j*Nb:(j+1)*Nb] = u
        
        if self.type == "Triangular" or self.type == "Hexagonal":
            # cut two small triangles
            for i in range(2*self.Nb):
                uplot[2*Nb+i,4*Nb-i-1:] = 0
                uplot[2*Nb-i-1,:i] = 0
        elif self.type == "Square":
            uplot[0, Nb//2] = 0 # put a zero somewhere
        if ax == None:
            plt.contourf(self.xWSplot, self.yWSplot, uplot, levels = 300,  cmap='afmhot_r')
        else:
            pcm = ax.contourf(self.xWSplot, self.yWSplot, uplot, levels = 300,  cmap='afmhot_r')
            return pcm
        
    def zoom_plot(self,u, ax = None): #for triangular lattice
        Nb = np.shape(u)[0]
        
        xxplot = np.linspace(-1/2, 1/2-1/Nb, Nb) #expand in 3 dimensions to plot
        xplotgrid, yplotgrid = np.meshgrid(xxplot, xxplot)
        xZplot = xplotgrid*self.a1[0] + yplotgrid*self.a2[0]
        yZplot = xplotgrid*self.a1[1] + yplotgrid*self.a2[1]
        
        
        uplot = np.zeros((Nb, Nb))
        # fill uplot
        uplot[:Nb//2, :Nb//2] = u[Nb//2:, Nb//2:]
        uplot[Nb//2:, Nb//2:] = u[:Nb//2, :Nb//2]
        
        for i in range(Nb):
                uplot[Nb+i,Nb-i-1:] = 0
                uplot[Nb-i-1,:i] = 0
        if ax == None:
            plt.contourf(xZplot, yZplot, uplot, levels = 300,  cmap='afmhot_r')
        else:
            pcm = ax.contourf(xZplot, yZplot, uplot, levels = 300,  cmap='afmhot_r')
            return pcm

###################################
# Other tools
##############################

##################################
# The multiplication by V operator
def get_Vop(V):
    Nb = np.shape(V)[0]
    Vl = np.reshape(V, Nb**2)
    def mult_V(ul):
        u = np.reshape(ul, (Nb, Nb))
        Vu = V*u
        return np.reshape(Vu, Nb**2)
    return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_V)

def get_V0(rBZ, I, Nb):
    # returns a correct first initial potential for neigs = 1
    tt = np.linspace(0, 1-1/Nb, Nb)
    xgrid, ygrid = np.meshgrid(tt, tt)
    if rBZ.type == "Triangular":
        u = np.cos(2*np.pi*(xgrid + ygrid))
    elif rBZ.type == "Square":
        u = np.cos(2*np.pi*xgrid)
    elif rBZ.type == "Hexagonal":
        u = 1 - np.cos(2*np.pi*(xgrid + ygrid))

    Ru = rBZ.average_transform(u)
    return -I*np.exp(2*Ru)



def Lp_norm(V, p):
    Nb = np.shape(V)[0]
    I = np.sum(abs(V)**p)/(Nb**2)
    return I**(1/p)

#######################################
# The LT semi-classical constant
def Lsc(kappa):
    return 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2) #semi classical cst


def compute_rho(V, rBZ, kappa, neigs = 2, logfile = None):
    # V is the potential evaluated on a grid
    # returns the potential rho, the sum of the eigenvalues**kappa, and the kinetic energy
    
    Nb, Vop = np.shape(V)[0], get_Vop(V)
    rBZ.compute_mDeltaq(Nb)

    rho = np.zeros((Nb, Nb))
    sum_eigs = 0
    kinetic = 0

    v0 = np.ones(Nb**2) #Starting vector for diagonalization

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        w, v = LA.eigsh(H, neigs, v0 = v0, which='SA')
        if neigs == 1: u0 = v[:,0]
        else : u0 = v[:,0] + v[:,1]

        for ie in range(neigs):
            lambdaq, uq = w[ie], v[:,ie] # only first negative eigenvalues

            K = np.real( np.dot(np.conj(uq), mDeltaq.dot(uq)) )

            if lambdaq < 0:
                kinetic +=  abs(lambdaq)**(kappa - 1) * K * rBZ.weights[iq]
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
                rho += abs(lambdaq)**(kappa - 1)* np.reshape(abs(uq)**2, (Nb,Nb)) * rBZ.weights[iq] * Nb

    return rBZ.average_transform(rho), sum_eigs, kinetic


################################
def get_info(V, rBZ, kappa, neigs = 2):
    Nb, Vop = np.shape(V)[0], get_Vop(V)
    rBZ.compute_mDeltaq(Nb)

    eigs = np.zeros( ( len(rBZ.rBZ) , neigs) )
    sum_eigs = 0

    v0 = np.ones(Nb**2) #Starting vector for diagonalization

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        #w, _ = np.linalg.eigh(H)
        w, v = LA.eigsh(H, neigs, v0 = v0, which='SA')
        eigs[iq, :] = w[:neigs]

        if neigs == 1: u0 = v[:,0]
        else : u0 = v[:,0] + v[:,1] 

        for ie in range(neigs):
            lambdaq = w[ie]
            if lambdaq < 0:
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
    #
    integral = Lp_norm(V, kappa+1)
    Llt = sum_eigs/(integral**(kappa+1))
    print("For kappa = {}".format(kappa))
    print("sum_eigs = {}, integral = {}, L/Lsc > {}".format( sum_eigs, integral, Llt/Lsc(kappa)))
    return eigs, Llt

#############################


def LT_constraint(V0, rBZ, kappa, integral, neigs = 2, tol=1e-6, Niter = 20, logfile=None) :
    # Returns V, sum_eigs, integral.
    # V is the self-consistent optimal potential
    # sum_eigs is \int |\lambda(q)|^\kappa
    # integral is \| V \|_{\kappa + 1}
    # L is therefore greater than sum_eigs/I

    # Initialisation
    Nb = np.shape(V0)[0]
    rBZ.compute_mDeltaq(Nb)

    print( "\nOptimisation with constraint integral = {}, and kappa = {}.".format(integral, kappa))
    print( "Lsc = {}".format( Lsc(kappa))) 
    

    In = Lp_norm(V0, kappa+1)
    Vn = (integral/In)*V0 #normalize V.

    rhon, sum_eigs, _ = compute_rho(Vn, rBZ, kappa, logfile=logfile)
    list_V, list_Llt = [], []

    # main loop
    for n in range(Niter):
        Vnp1 = - rhon**(1/(kappa)) #1/(kappa + d/2 - 1)
        # normalisation
        In = Lp_norm(Vnp1, kappa+1)
        Vnp1 = (integral/In)*Vnp1

        rhon, sum_eigs, _ = compute_rho(Vnp1, rBZ, kappa, neigs, logfile)
        Llt = sum_eigs/(integral**(kappa+1))

        print("\tIteration {}, I = {:6f}, kappa = {}, LT/Lsc = {}".format(n, integral, kappa, Llt/Lsc(kappa)))
        
        if np.linalg.norm(Vnp1 - Vn)/Nb < tol:
            print("Convergence after {} iterations".format(n))
            return Vnp1, Llt, list_V, list_Llt
        list_V.append(Vn)
        list_Llt.append(Llt)
        Vn = Vnp1
    print("No convergence after {} iterations".format(Niter))
    return Vnp1, Llt, list_V, list_Llt