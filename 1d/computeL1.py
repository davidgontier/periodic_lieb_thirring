# -*- coding: utf-8 -*-
import numpy as np
import scipy.integrate as integrate
import scipy.special as special


### resout l'equation NLS u''+(d-1)u'/r+u^{2p-1}- u=0 
### calcule la constante de Gagliardo-Nirenberg L_{\kappa,d}^{(1)}

d = 2 # the dimension I am interested in

#résolution de l'EDO à u0 et mu donnés
# u0 is the value at r = 0, d is the dimension
def solve_NLS(u0, kappa, d = d, Rmin = 1e-8, Rmax = 30):
    Y0=[u0,0] # initial condition
    p = (kappa+d/2)/(kappa+d/2 - 1) # the exponent p

    # the function
    def F(t, Y):
        return [Y[1], (1-d)*Y[1]/t+(-abs(Y[0])**(2*p-2)+1)*Y[0]]

    sol=integrate.solve_ivp(F, [Rmin, Rmax], Y0, dense_output=True, atol=1e-8, rtol=1e-8)
    return sol


#shooting method pour trouver la solution u qui tend vers 0 a l'infini
def find_best_u0(kappa, d = d, tol=1e-14, Niter = 100):
    u0_min=1
    u0_max=5
   
    rr = np.linspace(0, 10, 100) # create a grid
    #simple dichotomie method
    for _ in range(Niter):
        u0 = (u0_min + u0_max)/2

        # check tolerance
        if abs(u0_max - u0_min) < tol:
            return u0
        
        #update
        sol = solve_NLS(u0, kappa, d)

        if min(sol.sol(rr)[0]) < 0: # the solution becomes negative, u0 is too big
            u0_max = u0
        else:
            u0_min = u0
    print("no convergence after {} iterations".format(Niter))
    return u0

#mesure de l'hypersphere de dimension d-1
def Sd(d):
    return 2*np.pi**(d/2)/special.gamma(d/2)

# Compute L1
def L1(kappa, d = d):
    p=(kappa+d/2)/(kappa+d/2 - 1)
    u0 = find_best_u0(kappa, d)

    sol = solve_NLS(u0, kappa, d = d)

    # compute integral
    Rmax, Npts = 10, 100000
    p = (kappa + d/2)/(kappa + d/2 - 1)
    rr = np.linspace(1/(2*Npts), Rmax - 1/(2*Npts), Rmax*Npts)
    GN = sum( abs( sol.sol(rr)[0] )**(2*p)*rr )/Npts*Sd(d)

    #GN = sum( abs(valsol)**(2*p)*rr**(d-1)*Rmax)/Npts*Sd(d)
    return 1/GN