####################################
# 2d periodic LT
# Written by D. Gontier on September 8 2020

import numpy as np
import numpy.fft as fft
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special


##############################
# This is the code for the 2dimensional LT
##############################

def myprint(s, logfile):
    print(s)
    if logfile != None:
        with open(logfile, "a") as file:
            file.write("\n"+s)

###############################
# Brillouin zone
##############################

class BZ:
    def __init__(self, Nq):

        self.a = np.sqrt(2/np.sqrt(3))
        # The dual lattice
        self.Nq = Nq
        self.v1 = 2*np.pi*np.array([1, -1/np.sqrt(3)]) / self.a
        self.v2 = 2*np.pi*np.array([0, 2/np.sqrt(3)]) / self.a

        self.BZ = self.get_BZ()
        self.rBZ = self.get_rBZ()
        self.weights = self.get_weights()
        self.Nb = None


    def get_BZ(self):
        BZ = []
        qq = np.linspace(0, 1-1/self.Nq, self.Nq)
        for q1 in qq:
           for q2 in qq:
                q = q1*self.v1 + q2*self.v2
                BZ.append(q)    
        return BZ


    # We restrict our attention to potential that are 
    # 1/ even V(x1, x2) = V(-x1, x2), and 
    # 2/ Invariant by 2\pi/3 rotations: V(Rx) = V(x)
    def get_rBZ(self):
        rBZ = []
        tol=1e-7
    
        for q in self.BZ:
            if q[0] > -tol and q[0] < q[1]/np.sqrt(3)+tol and q[1] < self.v2[1]/2+tol:
                rBZ.append(q)
        return rBZ



    
    def get_weights(self):
        weights = []
        tol = 1e-7
        for q in self.rBZ:
            # first the special points
            if np.linalg.norm(q) < tol: #the center point
                weights.append(1)
            elif np.linalg.norm(q - self.v2-(self.v1 - self.v2)/3) < tol: # the "corner" of the BZ
                weights.append(2)
            elif np.linalg.norm(q - self.v2/2) < tol: # the "middle segment" of the BZ
                weights.append(3)
            elif abs(q[0]) < tol: # the horizontal line
                weights.append(6)
            elif abs(q[1] - np.sqrt(3)*q[0]) < tol: # the "diagonal" line
                weights.append(6)
            elif abs(q[1] - self.v2[1]/2) < tol: # the upper line
                weights.append(6)
            else:
                weights.append(12)
        return np.array(weights)/(self.Nq**2) #(normalisation)

    ###############################
    def get_kinetic_matrix(self):
        # k is the kpts in the Brillouin zone
        # Nb is the size of the basis per direction, so Nb**2 elements.

        KK = np.linspace(0, Nb-1, Nb)
        kx, ky = np.meshgrid(KK, KK)

        n1 = (kx*v1[0] + ky*v2[0] - k[0])**2 + (kx*v1[1] + ky*v2[1] - k[1])**2
        n2 = ((kx-Nb)*v1[0] + ky*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + ky*v2[1] - k[1])**2
        n3 = (kx*v1[0] + (ky-Nb)*v2[0] - k[0])**2 + (kx*v1[1] + (ky - Nb)*v2[1]- k[1])**2
        n4 = ((kx-Nb)*v1[0] + (ky - Nb)*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + (ky - Nb)*v2[1] - k[1])**2

        return np.minimum( np.minimum(n1, n2), np.minimum(n3, n4))

    def get_mDelta_q(self, q, Nb):
        # return (- i \nabla + q)^2 in Fourier
        # The operation is diagonal in Fourier
        # This is the multiplication by kinetic_q 
    
        # Construct kinetic_q
        KK = np.linspace(0, Nb-1, Nb)
        kx, ky = np.meshgrid(KK, KK)
        v1, v2 = self.v1, self.v2
        k = q

        n1 = (kx*v1[0] + ky*v2[0] - k[0])**2 + (kx*v1[1] + ky*v2[1] - k[1])**2
        n2 = ((kx-Nb)*v1[0] + ky*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + ky*v2[1] - k[1])**2
        n3 = (kx*v1[0] + (ky-Nb)*v2[0] - k[0])**2 + (kx*v1[1] + (ky - Nb)*v2[1]- k[1])**2
        n4 = ((kx-Nb)*v1[0] + (ky - Nb)*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + (ky - Nb)*v2[1] - k[1])**2

        _kinetic_q = np.minimum( np.minimum(n1, n2), np.minimum(n3, n4))

        def mult_mDelta(u):
            ul = np.reshape(u, (Nb, Nb))
            U = fft.ifft2(ul)
            mDeltaU = _kinetic_q*U
            mdeltaul = fft.fft2(mDeltaU)
            mdeltau = np.reshape(mdeltaul, Nb**2)
            return mdeltau
        
        M = np.zeros((Nb**2, Nb**2), dtype=complex)
        for i in range(Nb**2):
            ui = np.zeros(Nb**2)
            ui[i] = 1
            M[:, i] = mult_mDelta(ui)
        return M
        #return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_mDelta)

    ############################
    def compute_mDeltaq(self, Nb):
        if self.Nb == Nb:
            return
        
        self.Nb = Nb
        self.list_mDeltaq = []
        for q in self.rBZ:
        #for q in self.BZ:
            mDeltaq = self.get_mDelta_q(q, Nb)
            self.list_mDeltaq.append(mDeltaq)


#######################################
# A class for WS. We encode the different transformations
#######################################
class WS():

    def __init__(self, Nb):    
        self.a = np.sqrt(2/np.sqrt(3))
        self.a1 = self.a*np.array([1,0])
        self.a2 = self.a*np.array([1/2, np.sqrt(3)/2])


        self.Nb = Nb
        xx = np.linspace(0, 1-1/Nb, Nb)
        xgrid, ygrid = np.meshgrid(xx, xx)
        self.xWS = xgrid*self.a1[0] + ygrid*self.a2[0]
        self.yWS = xgrid*self.a1[1] + ygrid*self.a2[1]

        xxplot = np.linspace(-1, 2-1/Nb, 3*Nb) #expand in 3 dimensions to plot
        xplotgrid, yplotgrid = np.meshgrid(xxplot, xxplot)
        self.xWSplot = xplotgrid*self.a1[0] + yplotgrid*self.a2[0]
        self.yWSplot = xplotgrid*self.a1[1] + yplotgrid*self.a2[1]

    def plot(self,u):
        Nb = np.shape(u)[0]
        uplot = np.zeros((3*Nb, 3*Nb))
        for i in range(3):
            for j in range(3):
                uplot[i*Nb:(i+1)*Nb, j*Nb:(j+1)*Nb] = u
        plt.contourf(self.xWSplot, self.yWSplot, uplot)

###################################
# Other tools
##############################
def reflect(u):
    # u is defined on the WS grid
    # transforms a1 --> a1 and a2 --> a1 - a2
    Nb = np.shape(u)[0]
    newu = np.zeros((Nb,Nb))
    for i in range(Nb):
        for j in range(Nb):
            newu[i,j] = u[(-i)%Nb, (i+j)%Nb]
    return newu


def rotate(u):
    # rotation by 2*\pi/3 
    # transforms a1 --> a2-a1 and a2 --> -a1.
    Nb = np.shape(u)[0]
    newu = np.zeros((Nb, Nb))
    for i in range(Nb):
        for j in range(Nb):
            newu[i,j] = u[(-i-j)%Nb, i%Nb]
    return newu

def average_transform(u): 
    #returns the average of all 12 transformations
    R1u = rotate(u)
    R2u = rotate(R1u)

    newu = u.copy() + R1u + R2u # rotations
    newu += reflect(newu) # 
    return 2*np.real(newu)/12



##################################
# The multiplication by V operator
def get_Vop(V):
    Nb = np.shape(V)[0]
    Vl = np.reshape(V, Nb**2)
    #def mult_V(ul):
    #    u = np.reshape(ul, (Nb, Nb))
    #    Vu = V*u
    #    return np.reshape(Vu, Nb**2)
    #return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_V)
    VM = np.zeros((Nb**2, Nb**2))
    for i in range(Nb**2):
        VM[i,i] = Vl[i]
    return VM

def Lp_norm(V, p):
    Nb = np.shape(V)[0]
    I = np.sum(abs(V)**p)/(Nb**2)
    return I**(1/p)

#####################################
# From rho, the best V is of the form V = \lambda \rho^{1/\kappa}
# Here, we fix I = \int V^{\kappa + d/2}, and d/2 = 1
#def get_V_LT(rho, kappa, Itarget):
    # I is \int V^{\kappa + 1}
#    V = rho**(1/kappa)
#    I = sum(V**(kappa+1))/Nb**2
#    return V*(Itarget/I)**(1/(kappa+1))

#######################################
# The Hamiltonian operator
#def get_H_LT(k, V):
#    Nb = np.shape(V)[0]
#    mDelta = get_mDelta(k, Nb)
#    Vop = get_Vop(V)
#    return mDelta - Vop #sign - since Vop is positive

#######################################
# The LT semi-classical constant
def Lsc(kappa):
    return 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2) #semi classical cst


def compute_rho(V, rBZ, kappa, neigs = 2, logfile = None):
    # V is the potential evaluated on a grid
    # returns the potential rho, the sum of the eigenvalues**kappa, and the kinetic energy
    
    Nb, Vop = np.shape(V)[0], get_Vop(V)

    rho = np.zeros((Nb, Nb))
    sum_eigs = 0
    kinetic = 0

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        #w, v = LA.eigsh(H, 2, v0 = np.ones(Nb**2), which='SA')
        w, v = np.linalg.eigh(H)

        for ie in range(neigs):
            lambdaq, uq = w[ie], v[:,ie] # only first negative eigenvalues

            K = np.real( np.dot(np.conj(uq), mDeltaq.dot(uq)) )

            if lambdaq < 0:
                kinetic +=  abs(lambdaq)**(kappa - 1) * K * rBZ.weights[iq]
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
                rho += abs(lambdaq)**(kappa - 1)* np.reshape(abs(uq)**2, (Nb,Nb)) * rBZ.weights[iq] * Nb

    return average_transform(rho), sum_eigs, kinetic


################################
def get_info(V, rBZ, kappa, neigs = 2):
    Nb, Vop = np.shape(V)[0], get_Vop(V)
    rBZ.compute_mDeltaq(Nb)

    eigs = np.zeros( ( len(rBZ.rBZ) , neigs) )
    sum_eigs = 0

    for iq in range( len(rBZ.rBZ) ):
        mDeltaq = rBZ.list_mDeltaq[iq]
        H = mDeltaq + Vop

        # diagonalization
        w, _ = np.linalg.eigh(H)
        eigs[iq, :] = w[:neigs]
 
        for ie in range(neigs):
            lambdaq = w[ie]
            if lambdaq < 0:
                sum_eigs += abs(lambdaq)**kappa * rBZ.weights[iq]
    #
    integral = Lp_norm(V, kappa+1)
    Llt = sum_eigs/(integral**(kappa+1))
    print("For kappa = {}".format(kappa))
    print("sum_eigs = {}, integral = {}, L/Lsc > {}".format( sum_eigs, integral, Llt/Lsc(kappa)))
    return eigs, Llt

#############################


def LT_constraint(V0, rBZ, kappa, integral, neigs = 2, tol=1e-6, Niter = 20, logfile=None) :
    # Returns V, sum_eigs, integral.
    # V is the self-consistent optimal potential
    # sum_eigs is \int |\lambda(q)|^\kappa
    # integral is \| V \|_{\kappa + 1}
    # L is therefore greater than sum_eigs/I

    # Initialisation
    Nb = np.shape(V0)[0]
    rBZ.compute_mDeltaq(Nb)

    myprint( "\nOptimisation with constraint integral = {}, and kappa = {}.".format(integral, kappa) , logfile)
    myprint( "Lsc = {}".format( Lsc(kappa)) , logfile ) 
    

    In = Lp_norm(V0, kappa+1)
    Vn = (integral/In)*V0 #normalize V.

    rhon, sum_eigs, _ = compute_rho(Vn, rBZ, kappa, logfile=logfile)
    list_V, list_Llt = [], []

    # main loop
    for n in range(Niter):
        Vnp1 = - rhon**(1/(kappa)) #1/(kappa + d/2 - 1)
        # normalisation
        In = Lp_norm(Vnp1, kappa+1)
        Vnp1 = (integral/In)*Vnp1

        rhon, sum_eigs, _ = compute_rho(Vnp1, rBZ, kappa, neigs, logfile)
        Llt = sum_eigs/(integral**(kappa+1))

        myprint("\tIteration {}, kappa = {}, LT/Lsc = {}".format(n, kappa, Llt/Lsc(kappa)) , logfile)

        
        if np.linalg.norm(Vnp1 - Vn)/Nb < tol:
            myprint("Convergence after {} iterations".format(n) , logfile)
            return Vnp1, Llt, list_V, list_Llt
        list_V.append(Vn)
        list_Llt.append(Llt)
        Vn = Vnp1
    myprint("No convergence after {} iterations".format(Niter) , logfile)
    return Vnp1, Llt, list_V, list_Llt