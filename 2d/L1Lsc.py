# -*- coding: utf-8 -*-
"""
Created on August 19, 2020

@author: mathieu
"""


from numpy import *
from scipy.integrate import odeint
from scipy.special import gamma
#from scipy.optimize import fsolve
#from numpy.linalg import norm, eigh
### resout l'equation NLS u''+(d-1)u'/r+u^{2p-1}- u=0 
### calcule la constante de Gagliardo-Nirenberg L_{\kappa,d}^{(1)}
### compare avec la cosntante semi-classique L_{\kappa,d}^{sc}
### comme dans appendice Barnes de Lieb-Thirring 1976
###
#variables globales
Rmax=30 #résolution de l'EDO sur [0,Rmax]
N = 1e5 #Nb de points de discrétisation
tol=1e-9 #tolerance pour shooting
tol_kappa=1e-6 #tolerance pour kappa_c
###
### calcul de divers quantités utiles pour la suite
rr = arange(Rmax/N,Rmax*(N+1)/N,Rmax/N)
rr = linspace(1e-8, Rmax - 1/N, N*Rmax)
##

##calcule l'exposant p en fonction de kappa et d
def pp(d, kappa):
    return (kappa+d/2)/(kappa+d/2-1)

#fonction non linéaire écrite sous forme matricielle
def F(Y, t, d, kappa):
    p=pp(d, kappa)
    return [Y[1], (1-d)*Y[1]/t+(-abs(Y[0])**(2*p-2)+1)*Y[0]]

#résolution de l'EDO à u0 et mu donnés
def solve_NLS(u0, d, kappa):
    Y0=[u0,0]
    sol=odeint(F, Y0, rr, args=(d,kappa))
    return sol

#shooting method pour trouver la solution u qui tend vers 0 a l'infini
def u_du_sol(d, kappa, tol):
    u0_min=1
    u0_max=5
    
    while u0_max-u0_min>tol:
        u0=(u0_max+u0_min)/2
        sol=solve_NLS(u0, d, kappa)
        u=sol[:,0]
        du=sol[:,1]
        if min(u)<0:
            u0_max=u0
        else:
            u0_min=u0
    return u,du

#calcule et trace la solution de NLS
def trace_u_sol(d, kappa):
    u,du=u_du_sol(d, kappa,tol)
    plot(rr,u)
    return u

#mesure de l'hypersphere de dimension d-1
def Sd(d):
    return 2*pi**(d/2)/gamma(d/2)

#calcule la constante de Gagliardo-Nirenberg
def L1(d, kappa):
    p=pp(d, kappa)
    u,du=u_du_sol(d, kappa, tol)
    #u=trace_u_sol(d, kappa) #(pour verification)
    GN=dot(abs(u)**(2*p),rr**(d-1))*Rmax/N*Sd(d)
    return 1/GN

#calule la constante semi-classique
def Lsc(d, kappa):
    return 2**(-d)*pi**(-d/2)*gamma(1+kappa)/gamma(1+d/2+kappa)

#calcule la constante de Sobolev L1(d,0) Aubin-Talenti
def Lsob(d):
    Csob=pi**(-1/2)*d**(-1/2)(d-1)**(-1/2)*(gamma(1+d/2)*gamma(d)/gamma(d/2)/gamma(1+d/2))**(1/d)

#calcule L1 et Lsc pour tous les kappa dans K
def L1Lsc(d,K):
    LL1=[L1(d, K[i]) for i in range(len(K))]
    LLsc=[Lsc(d, K[i]) for i in range(len(K))]
    return LL1, LLsc

#calcule L1 / Lsc pour tous les kappa dans K
def L1_over_Lsc(d,K):
    LL1=[L1(d, K[i]) for i in range(len(K))]
    LLsc=[Lsc(d, K[i]) for i in range(len(K))]
    return divide(LL1,LLsc)

#trace les courbes voules
def trace_L1_over_Lsc(D,K):
    for i in range(len(D)):
        plot(K,L1_over_Lsc(D[i],K))
    plot(K,ones(len(K)),'k')

#trouve kappa_c en commençant sur l'intervalle [k_min,k_max]
def kappa_c(d,k_min,k_max):
    print('Cherche kappa_c en dimension ',d)
    L_min=L1(d,k_min)/Lsc(d,k_min)
    L_max=L1(d,k_max)/Lsc(d,k_max)
    if L_min<1 or L_max>1:
        print('Wrong interval')
        quit()
    while k_max-k_min>tol:
        k_new=(k_min+k_max)/2
        L_new=L1(d,k_new)/Lsc(d,k_new)
        if L_new>1:
            k_min=k_new
        else:
            k_max=k_new
    return k_new
    
#######################################################
##### PROGRAMME PRINCIPAL
#######################################################

######################################################
#####trace et sauve les courbes voulues
######################################################
#D=[4, 5, 6, 7] #dimensions
#K=arange(0.01,1.3,0.01)
#trace_L1_over_Lsc(D,K)
##(les courbes de d=2 et d=3 montent un peu trop haut, on les tronque)
#K2=arange(0.2,1.3,0.01)
#plot(K2,L1_over_Lsc(2,K2),'r')
#K3=arange(0.07,1.3,0.01)
#plot(K3,L1_over_Lsc(3,K3),'m')
#savefig('figure.eps')
#savefig('figure.pdf')

######################################################
#####trouve kappa_sc(d)
######################################################
#d,k_min,k_max=2,1.1653,1.1655
#d,k_min,k_max=3,0.862,0.863
#d,k_min,k_max=4,0.597,0.599
#d,k_min,k_max=5,0.373,0.375
#d,k_min,k_max=6,0.19,0.21
#d,k_min,k_max=7,0.067,0.070
#print(N,' points and Rmax=',Rmax)
#print(kappa_c(d,k_min,k_max))

######################################################
#####calcule et dessine la solution de NLS
######################################################
#d=2
#kappa=1 #puissance
###trace_u_sol(d, kappa)
#print(L1_over_Lsc(d,[kappa]))

