import multiprocessing
from multiprocessing import Pool
import numpy as np
import periodicLT_2d as LT
import datetime


######################################
# The parameters
######################################
Nb = 40 # number of points in [-1/2, 1/2] for discretization
Nq = 30 # number of q point for BZ integration


kappakappa = [1.165417]
#  The best kappa is in 
# Triangular:        1.165418 , I around 28.7
# Square:            1.165395, with I around 33.1
# Hexagonal:         1.165389, with I around 77.2

integral_min = 25 # minimum value of the integral
integral_max = 55
Nintegral = 61 # number of points in Integral

tol = 1e-6# precision for sc loop
Niter = 100 # number of iteration for the sc loop

geometry = "Hexagonal"
neigs = 2 #Attention, j'ai modifié II aussi
II = 2*np.linspace(integral_min, integral_max, Nintegral) #list of integrals

##################################

now = datetime.datetime.now()
kappa = kappakappa[0]
strnow = '{:02d}-{:02d}_{:02d}H{:02d}_{}_kappa_{:d}'.format(now.month, now.day, now.hour, now.minute, geometry, int(1e6*kappa))

filename = "Data/out_"+strnow
logfilename = "Data/log_"+strnow

########################################
Nkappa = len(kappakappa)
Ncpu = multiprocessing.cpu_count()

if Ncpu==4: #local computation
    filename += "_local"
    logfilename += "_local"

logfilename += ".txt"

#####################################
########################################
print("\nComputing rBZ")
rBZ = LT.BZ(Nq, type=geometry) # Initialisation of the BZ locally
rBZ.compute_mDeltaq(Nb)
print("Done. Size rBZ = {}".format(len(rBZ.rBZ)))


########################################
# Get V0
#######################################
def load_V0(I, Nb):
    if geometry == "Triangular" or geometry == "Square":
        newI = int(I/2)*2
    else:
        newI = int(I/4)*4
    
    name = "Data/V0/V0_I{:d}_{}.npy".format(newI, geometry)
    V0 = np.load(name)
    return V0



#########################################
# Multiprocessing: each process runs the code with a different kappa, and for all value I
# We create a function which depends on kappa only
#########################################
def bestLT_I(args, rBZ = rBZ, logfilename = logfilename):

    # II is a list of integrals.
    # Returns a list [V(kappa, i1), ... V(kappa, iN)], where I = [i1,...,iN]
    kappa, I = args
    #V = -I*np.ones((Nb, Nb)) + I/2*np.random.random((Nb, Nb)) # initial random potential
    #V0 = LT.get_V0(rBZ, I, Nb)
    V0 = load_V0(I, Nb)
    V, Llt, _, _ = LT.LT_constraint(V0, rBZ, kappa, I, neigs = neigs, tol=tol, Niter=Niter, logfile=logfilename)

    s = "result\t{}\t{}\t{}".format(I, kappa, Llt)
    print(s)
    with open(logfilename, "a") as file:
        file.write("\n" + s)

    return (Llt, V)


##########################
### MAIN #################
print("Ncpu = ", Ncpu)
print("Nb = ", Nb)
print("Nq = ", Nq)
print("geometry = ", geometry)


s = "Geometry = {}\n\n I \t kappa \t Llt.\n ------------------------------------\n".format(geometry)

print(s)
with open(logfilename, "a") as file:
    file.write("\n" + s)

# prepare computation
kk, ii = np.meshgrid(kappakappa, II)
kk, ii = list( np.reshape(kk, Nkappa*Nintegral ) ), list( np.reshape(ii, Nkappa*Nintegral ) ) # as list
args = [(kk[i], ii[i]) for i in range(len(kk))]

with Pool(Ncpu) as pool:
    final_data = pool.map(bestLT_I, args)

data = {
    "geometry": geometry,
    "Nb": Nb,
    "Nq": Nq,
    "neigs": neigs,
    "kappakappa": kappakappa,
    "II": II,
    "final_data": final_data,
}

np.save(filename+".npy", data) # save

print("Done. Results in {}".format(filename))